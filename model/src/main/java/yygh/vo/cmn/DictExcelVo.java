package yygh.vo.cmn;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 11:53
 */
@Data
public class DictExcelVo {

    @ExcelProperty(value = "id" ,index = 0)
    private Long id;

    @ExcelProperty(value = "上级id" ,index = 1)
    private Long parentId;

    @ExcelProperty(value = "名称" ,index = 2)
    private String name;

    @ExcelProperty(value = "值" ,index = 3)
    private String value;

    @ExcelProperty(value = "编码" ,index = 4)
    private String dictCode;

}
