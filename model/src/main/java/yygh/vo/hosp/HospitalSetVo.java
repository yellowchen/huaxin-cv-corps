package yygh.vo.hosp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Desc: 返回的前端的
 * @Author yu
 * @Date 2023/9/4 20:42
 */
@Data
@ApiModel(description = "医院设置Vo")
public class HospitalSetVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医院主键ID")
    private Long id;

    @ApiModelProperty(value = "医院名称")
    private String hosname;

    @ApiModelProperty(value = "医院编号")
    private String hoscode;

    @ApiModelProperty(value = "api基础路径")
    private String apiUrl;

    @ApiModelProperty(value = "联系人姓名")
    private String contactsName;

    @ApiModelProperty(value = "联系人手机")
    private String contactsPhone;

    @ApiModelProperty(value = "状态")
    private Integer status;

}
