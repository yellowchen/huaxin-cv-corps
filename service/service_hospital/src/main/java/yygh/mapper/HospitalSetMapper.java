package yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import yygh.pojo.hosp.HospitalSet;

@Mapper
@Component
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
