package yygh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yygh.pojo.hosp.HospitalSet;
import yygh.result.Result;
import yygh.vo.hosp.HospitalSetQueryVo;

import java.util.List;

public interface HospitalSetService extends IService<HospitalSet> {

    /**
     * @Desc: 查询医院设置表所有信息
     */
    Result findAll();

    /**
     * @Desc: 逻辑删除医院设置
     */
    Result removeHospSet(Long id);

    /**
     * @Desc: 条件查询带分页
     */
    Result findPageHospSet(Long current, Long limit, HospitalSetQueryVo hospitalSetQueryVo);

    /**
     * @Desc: 添加医院设置
     */
    Result saveHospitalSet(HospitalSet hospitalSet);

    /**
     * @Desc: 根据id获取医院设置
     */
    Result getHospSet(Long id);

    /**
     * @Desc: 修改医院设置
     */
    Result updateHospitalSet(HospitalSet hospitalSet);

    /**
     * @Desc: 批量删除医院设置
     */
    Result batchRemoveHospitalSet(List<Long> idList);

    /**
     * @Desc: 医院设置的锁定和解锁
     */
    Result lock(Long id, Integer status);


    /**
     * @Desc: 发送签名秘钥
     */
    Result sendKey(Long id);

    String getSignKey(String hoscodeParam);
}
