package yygh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yygh.mapper.HospitalSetMapper;
import yygh.mapstruct.MapStructs;
import yygh.pojo.hosp.HospitalSet;
import yygh.result.Result;
import yygh.result.ResultCodeEnum;
import yygh.service.HospitalSetService;
import yygh.utils.MD5;
import yygh.vo.hosp.HospitalSetQueryVo;
import yygh.vo.hosp.HospitalSetVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/4 20:54
 */
@Service
@Transactional
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService{

    @Autowired
    private HospitalSetMapper hospitalSetMapper;

    @Autowired
    private HospitalSetService hospitalSetService;


    /**
     * @Desc: 查询医院设置表所有信息
     */
    @Override
    public Result findAll() {
        List<HospitalSet> hospitalSets = hospitalSetService.list(new QueryWrapper<HospitalSet>().eq("is_deleted",0));
        List<HospitalSetVo> hospitalSetVos=new ArrayList<>();
        for (HospitalSet hospitalSet:hospitalSets){
            hospitalSetVos.add(MapStructs.INSTANCE.toHospitalSetVo(hospitalSet));
        }

        return Result.ok(hospitalSetVos);
    }

    /**
     * @param id
     * @Desc: 逻辑删除医院设置
     */
    @Override
    public Result removeHospSet(Long id) {
        boolean result = hospitalSetService.removeById(id);
        if(!result){
            return Result.fail();
        }
        return Result.ok();
    }

    /**
     * @param current
     * @param limit
     * @param hospitalSetQueryVo
     * @Desc: 条件查询带分页
     */
    @Override
    public Result findPageHospSet(Long current, Long limit, HospitalSetQueryVo hospitalSetQueryVo) {
        //创建page对象，传递当前页，每页记录数
        Page<HospitalSet> page = new Page<>(current,limit);
        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(hospitalSetQueryVo.getHoscode())){
            queryWrapper.like("hoscode",hospitalSetQueryVo.getHoscode());
        }
        if(!StringUtils.isBlank(hospitalSetQueryVo.getHosname())){
            queryWrapper.like("hosname",hospitalSetQueryVo.getHosname());
        }
        queryWrapper.eq("is_deleted",0);
        Page<HospitalSet> pageHospitalSet = hospitalSetService.page(page,queryWrapper);
        Page<HospitalSetVo> hospitalSetVoPage = new Page<>();

        List<HospitalSetVo> collect = pageHospitalSet
                .getRecords()
                .stream()
                .map(MapStructs.INSTANCE::toHospitalSetVo)
                .collect(Collectors.toList());
        hospitalSetVoPage.setRecords(collect);

        return Result.ok(hospitalSetVoPage);
    }

    /**
     * @param hospitalSet
     * @Desc: 添加医院设置
     */
    @Override
    public Result saveHospitalSet(HospitalSet hospitalSet) {
        HospitalSet hoscode = hospitalSetService.getOne(new QueryWrapper<HospitalSet>().eq("hoscode", hospitalSet.getHoscode()));
        if(hoscode!=null){
            return Result.build(ResultCodeEnum.FAIL.getCode(),"医院信息已存在");
        }
        //设置状态 1 使用 0 不能使用
        hospitalSet.setStatus(1);
        hospitalSet.setIsDeleted(0);
        //签名秘钥
        Random random = new Random();
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));

        boolean result = hospitalSetService.save(hospitalSet);
        if(!result){
            return Result.fail();
        }
        return Result.ok();
    }

    /**
     * @param id
     * @Desc: 根据id获取医院设置
     */
    @Override
    public Result getHospSet(Long id) {
        HospitalSet hospitalSet = hospitalSetService.getOne(new QueryWrapper<HospitalSet>().eq("id",id).eq("is_deleted",0));
        if(hospitalSet == null){
            return Result.build(ResultCodeEnum.DATA_ERROR.getCode(),"数据不存在");
        }
        return Result.ok(hospitalSet);
    }

    /**
     * @param hospitalSet
     * @Desc: 修改医院设置
     */
    @Override
    public Result updateHospitalSet(HospitalSet hospitalSet) {
        boolean result = hospitalSetService.updateById(hospitalSet);

        if(!result){
            return Result.fail();
        }
        return Result.ok();
    }

    /**
     * @param idList
     * @Desc: 批量删除医院设置
     */
    @Override
    public Result batchRemoveHospitalSet(List<Long> idList) {
        int i = hospitalSetMapper.delete(new QueryWrapper<HospitalSet>().in("id", idList));
        if(i==0){
            return Result.build(ResultCodeEnum.FAIL.getCode(),"批量删除失败");
        }
        return Result.ok(i);
    }

    /**
     * @param id 医院id
     * @param status 状态 0：锁定 ；1：解锁
     * @Desc: 医院设置的锁定和解锁
     */
    @Override
    public Result lock(Long id, Integer status) {
        HospitalSet byId = hospitalSetService.getOne(new QueryWrapper<HospitalSet>().eq("id",id));
        if(byId==null){
            return Result.build(ResultCodeEnum.FAIL.getCode(),"医院设置不存在");
        }
        byId.setStatus(status);
        boolean update = hospitalSetService.updateById(byId);
        if(!update){
            return Result.fail();
        }
        return Result.ok();
    }

    /**
     * @param id
     * @Desc: 发送签名秘钥
     */
    @Override
    public Result sendKey(Long id) {
        HospitalSet byId = hospitalSetService.getById(id);
        String signKey = byId.getSignKey();
        String hoscode = byId.getHoscode();
        //todo 发送短信待做


        return Result.ok();
    }

    @Override
    public String getSignKey(String hoscodeParam) {
        QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        wrapper.eq("hoscode",hoscodeParam);
        HospitalSet hospitalSet = baseMapper.selectOne(wrapper);
        return hospitalSet.getSignKey();
    }

}
