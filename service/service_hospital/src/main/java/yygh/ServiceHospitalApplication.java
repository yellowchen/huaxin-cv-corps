package yygh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/4 20:29
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.yuydot")
@EnableFeignClients(basePackages = "com.yuydot")
public class ServiceHospitalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospitalApplication.class,args);
    }
}
