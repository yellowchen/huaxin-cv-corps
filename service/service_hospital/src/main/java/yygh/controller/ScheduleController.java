package yygh.controller;

import com.yuydot.yygh.pojo.hosp.Schedule;
import com.yuydot.yygh.result.Result;
import com.yuydot.yygh.service.ScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/13 22:19
 */
@Api(tags = "排班管理")
@RestController
@RequestMapping("/admin/hosp/schedule")
//@CrossOrigin
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value = "查询排班规则数据")
    @GetMapping("getSechduleRule/{page}/{limit}/{hoscode}/{depcode}")
    public Result getSechduleRule(@PathVariable("page")int page,
                                  @PathVariable("limit")int limit,
                                  @PathVariable("hoscode")String hoscode,
                                  @PathVariable("depcode")String depcode){
        Map<String,Object> map = scheduleService.getSechduleRule(page,limit,hoscode,depcode);
        return Result.ok(map);
    }

    //根据医院编号、科室编号、工作日期查询详细排班信息
    @ApiOperation(value = "查询详细排班信息")
    @GetMapping("getSechduleDetail/{hoscode}/{depcode}/{workDate}")
    public Result getSechduleDetail(@PathVariable("hoscode")String hoscode,
                                  @PathVariable("depcode")String depcode,
                                  @PathVariable("workDate")String workDate){
        List<Schedule> list = scheduleService.getSechduleDetail(hoscode,depcode,workDate);
        return Result.ok(list);
    }

}
