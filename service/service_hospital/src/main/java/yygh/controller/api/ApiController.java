package yygh.controller.api;

import com.yuydot.yygh.MD5;
import com.yuydot.yygh.exception.YyghException;
import com.yuydot.yygh.helper.HttpRequestHelper;
import com.yuydot.yygh.pojo.hosp.Department;
import com.yuydot.yygh.pojo.hosp.Hospital;
import com.yuydot.yygh.pojo.hosp.Schedule;
import com.yuydot.yygh.result.Result;
import com.yuydot.yygh.result.ResultCodeEnum;
import com.yuydot.yygh.service.DepartmentService;
import com.yuydot.yygh.service.HospitalService;
import com.yuydot.yygh.service.HospitalSetService;
import com.yuydot.yygh.service.ScheduleService;
import com.yuydot.yygh.vo.hosp.DepartmentQueryVo;
import com.yuydot.yygh.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @version 1.0
 * @Desc: 平台对外开发的接口都写在该Controller类
 * @Author yu
 * @Date 2023/9/11 9:17
 */
@Api(tags = "医院管理API接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value = "上传医院接口")
    @PostMapping
    @RequestMapping("saveHospital")
    public Result saveHospital(HttpServletRequest request){
        Map<String, Object> paramMap = this.VerificationSign(request);
        //传输过程中，图片中的“+”变成了“ ”，因此我们要转回来
        String logoData = (String) paramMap.get("logoData");
        logoData = logoData.replaceAll(" ", "+");
        paramMap.put("logoData",logoData);

        //调用service方法
        hospitalService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "查询医院接口")
    @PostMapping("hospital/show")
    public Result getHospital(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);
        //调用service实现根据医院编号查询
        Hospital hospital = hospitalService.getByHoscode((String)paramMap.get("hoscode"));
        return  Result.ok(hospital);
    }


    @ApiOperation(value = "查询科室接口")
    @PostMapping("department/list")
    public Result getDepartment(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);
        //拿到page页和每页数量
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 :Integer.parseInt((String) paramMap.get("page"));
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));

        DepartmentQueryVo departmentQueryVo = new DepartmentQueryVo();
        departmentQueryVo.setHoscode((String)paramMap.get("hoscode"));

        Page<Department> pageModel = departmentService.findPageDepartment(page,limit,departmentQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "上传科室接口")
    @PostMapping("saveDepartment")
    public Result saveDepartment(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);

        departmentService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "删除科室接口")
    @PostMapping("department/remove")
    public Result removeDepartment(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);
        String hoscode = (String) paramMap.get("hoscode");
        String depcode = (String) paramMap.get("depcode");
        departmentService.remove(hoscode,depcode);
        return Result.ok();
    }

    @ApiOperation(value = "上传排班接口")
    @PostMapping("saveSchedule")
    public Result saveSchedule(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);

        scheduleService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "查询排班接口")
    @PostMapping("schedule/list")
    public Result getSchedule(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);
        //拿到page页和每页数量
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 :Integer.parseInt((String) paramMap.get("page"));
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));

        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        scheduleQueryVo.setHoscode((String)paramMap.get("hoscode"));
        scheduleQueryVo.setDepcode((String)paramMap.get("depcode"));

        Page<Schedule> pageModel = scheduleService.findPageSchedule(page,limit,scheduleQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "删除排班接口")
    @PostMapping("schedule/remove")
    public Result removeSchedule(HttpServletRequest request){
        //验证签名
        Map<String, Object> paramMap = this.VerificationSign(request);
        String hoscode = (String) paramMap.get("hoscode");
        String hosScheduleId = (String) paramMap.get("hosScheduleId");
        scheduleService.remove(hoscode,hosScheduleId);
        return Result.ok();
    }

    /**
     * @Desc: 验证签名
     * @Param: null
     * @Return:
     */
    private Map<String,Object> VerificationSign(HttpServletRequest request){
        //获取医院传递的医院信息
        Map<String,String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //验证签名
        //1. 获取医院系统传递的签名，签名进行MD5加密
        String hospSignParam = (String) paramMap.get("sign");

        //2.通过传递的医院信息从数据库中获取签名；
        String hoscodeParam = (String) paramMap.get("hoscode");
        String signKey= hospitalSetService.getSignKey(hoscodeParam);

        //3.MD5加密hoscode
        String encrypt = MD5.encrypt(signKey);

        //4.比较签名
        if(!hospSignParam.equals(encrypt)){
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        return paramMap;
    }

}
