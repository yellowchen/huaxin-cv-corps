package yygh.controller;

import com.yuydot.yygh.pojo.hosp.Hospital;
import com.yuydot.yygh.result.Result;
import com.yuydot.yygh.service.HospitalService;
import com.yuydot.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Map;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/12 10:41
 */
@Api(tags = "医院管理")
@RestController
@RequestMapping("/admin/hosp/hospital")
//@CrossOrigin
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;


    @ApiOperation(value = "医院列表（条件查询分页）")
    @GetMapping("list/{page}/{limit}")
    public Result listHops(@PathVariable("page")Integer page,
                           @PathVariable("limit")Integer limit,
                           HospitalQueryVo hospitalQueryVo){
        Page<Hospital> pageModel = hospitalService.selectHospPage(page,limit,hospitalQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "更新医院上线状态")
    @GetMapping("updateHospStatus/{id}/{status}")
    public Result updateHospStatus(@PathVariable("id")String id,
                           @PathVariable("status")Integer status){
        hospitalService.updateStatus(id,status);
        return Result.ok();
    }

    @ApiOperation(value = "医院详情信息")
    @GetMapping("showHospDetail/{id}")
    public Result showHospDetail(@PathVariable("id")String id){
        Map<String,Object> map = hospitalService.showHospDetail(id);
        return Result.ok(map);
    }
}
