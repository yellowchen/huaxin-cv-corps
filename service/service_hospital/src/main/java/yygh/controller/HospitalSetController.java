package yygh.controller;

import com.yuydot.yygh.pojo.hosp.HospitalSet;
import com.yuydot.yygh.result.Result;
import com.yuydot.yygh.service.HospitalSetService;
import com.yuydot.yygh.vo.hosp.HospitalSetQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/4 20:56
 */
@Api(tags = "医院设置管理")
@RestController
@RequestMapping("/admin/hosp/hospitalSet")
//@CrossOrigin
public class HospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;


    @ApiOperation(value = "获取所有医院设置")
    @GetMapping("findAll")
    public Result findAllHospitalSet(){

        return hospitalSetService.findAll();
    }

    @ApiOperation(value = "逻辑删除医院设置")
    @DeleteMapping("/removeHospSet/{id}")
    public Result removeHospSet(@PathVariable Long id){
        return hospitalSetService.removeHospSet(id);
    }

    @ApiOperation(value = "条件查询带分页")
    @PostMapping("findPageHospSet/{current}/{limit}")
    public Result findPageHospSet(@PathVariable long current,
                                   @PathVariable long limit,
                                   @RequestBody
                                   (required = false) HospitalSetQueryVo hospitalSetQueryVo){

        return hospitalSetService.findPageHospSet(current,limit,hospitalSetQueryVo);
    }

    @ApiOperation(value = "添加医院设置")
    @PostMapping("saveHospitalSet")
    public Result saveHospitalSet(@RequestBody HospitalSet hospitalSet) {
         return hospitalSetService.saveHospitalSet(hospitalSet);
    }

    @ApiOperation(value = "根据id获取医院设置")
    @GetMapping("getHospSet/{id}")
    public Result getHospSet(@PathVariable Long id) {
        return hospitalSetService.getHospSet(id);
    }

    @ApiOperation(value = "修改医院设置")
    @PostMapping("updateHospitalSet")
    public Result updateHospitalSet(@RequestBody HospitalSet hospitalSet) {
        return hospitalSetService.updateHospitalSet(hospitalSet);
    }

    @ApiOperation(value = "批量删除医院设置")
    @DeleteMapping("batchRemove")
    public Result batchRemoveHospitalSet(@RequestBody List<Long> idList) {
        return hospitalSetService.batchRemoveHospitalSet(idList);
    }

    @ApiOperation(value = "医院设置的锁定和解锁 status 0：锁定 ；1：解锁")
    @PutMapping("lock/{id}/{status}")
    public Result lockHospitalSet(@PathVariable("id")Long id, @PathVariable("status")Integer status) {
        return hospitalSetService.lock(id,status);
    }

    @ApiOperation(value = "发送签名秘钥")
    @PostMapping("sendKey/{id}")
    public Result sendKey(@PathVariable("id")Long id) {
        return hospitalSetService.sendKey(id);
    }
}
