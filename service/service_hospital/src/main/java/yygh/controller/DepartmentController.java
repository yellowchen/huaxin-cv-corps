package yygh.controller;

import com.yuydot.yygh.result.Result;
import com.yuydot.yygh.service.DepartmentService;
import com.yuydot.yygh.vo.hosp.DepartmentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/13 16:10
 */
@RestController
@RequestMapping("/admin/hosp/department")
@Api(tags = "科室管理")
//@CrossOrigin
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;


    @ApiOperation(value = "查询医院所有科室列表")
    @GetMapping("getDeptList/{hoscode}")
    public Result getDeptList(@PathVariable("hoscode")String hoscode) {
        List<DepartmentVo> departmentVoList = departmentService.findDeptTree(hoscode);
        return Result.ok(departmentVoList);
    }

}
