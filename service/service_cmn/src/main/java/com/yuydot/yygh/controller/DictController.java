package com.yuydot.yygh.controller;


import com.yuydot.yygh.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import yygh.pojo.cmn.Dict;
import yygh.result.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 10:03
 */
@RestController
@RequestMapping("/admin/cmn/dict")
//@CrossOrigin
@Api("数据字典管理")
public class DictController {

    @Autowired
    private DictService dictService;

    @ApiOperation(value = "根据数据id查询子数据列表")
    @GetMapping("findChildData/{id}")
    public Result findChildData(@PathVariable("id") Long id){
        List<Dict> list = dictService.findChildData(id);
        return Result.ok(list);
    }

    @ApiOperation(value = "导出数据字典")
    @GetMapping("exportData")
    public Result exportData(HttpServletResponse response){
        dictService.exportDictData(response);
        return Result.ok();
    }
    

    @ApiOperation(value = "导入数据字典")
    @PostMapping("importData")
    public Result importData(MultipartFile file){
        dictService.importDictData(file);
        return Result.ok();
    }

    @ApiOperation(value = "根据dicCode和value查询")
    @GetMapping("getName/{dictCode}/{value}")
    public String getName(@PathVariable("dictCode")String dictCode,
                          @PathVariable("value")String value){
        String dictName=dictService.getDictName(dictCode,value);
        return dictName;
    }

    @ApiOperation(value = "根据value查询")
    @GetMapping("getName/{value}")
    public String getName(@PathVariable("value")String value){
        String dictName=dictService.getDictName("",value);
        return dictName;
    }

    @ApiOperation(value = "根据dictCode查询下级节点（获取医院等级）")
    @GetMapping("findByDictCode/{dictCode}")
    public Result findByDictCode(@PathVariable("dictCode")String dictCode){
        List<Dict> dictList = dictService.findByDictCode(dictCode);
        return Result.ok(dictList);
    }
}
