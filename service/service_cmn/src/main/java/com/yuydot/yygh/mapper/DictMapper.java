package com.yuydot.yygh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import yygh.pojo.cmn.Dict;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 10:00
 */
@Mapper
@Component
public interface DictMapper extends BaseMapper<Dict> {
}
