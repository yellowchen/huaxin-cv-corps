package com.yuydot.yygh.service.impl;

import com.alibaba.excel.EasyExcel;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuydot.yygh.listener.DictListener;
import com.yuydot.yygh.mapper.DictMapper;
import com.yuydot.yygh.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import yygh.mapstruct.MapStructs;
import yygh.pojo.cmn.Dict;
import yygh.vo.cmn.DictExcelVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 10:02
 */
@Service
@Transactional
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Autowired
    private DictMapper dictMapper;
    /**
     * @param id
     * @Desc: 根据数据id查询子数据列表
     */
    @Cacheable(value = "dict",keyGenerator = "keyGenerator")
    @Override
    public List<Dict> findChildData(Long id) {
        List<Dict> parent_id = dictMapper.selectList(new QueryWrapper<Dict>().eq("parent_id", id));
        //给list集合中每个hasChild设置值
        for (Dict dict : parent_id) {
            boolean child = this.isChild(dict.getId());
            dict.setHasChildren(child);
        }
        return parent_id;
    }

    /**
     * @Desc: 导入数据字典
     */
    @Override
    @CacheEvict(value = "dict", allEntries=true)
    public void importDictData(MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(), DictExcelVo.class,new DictListener(dictMapper))
                    .sheet()
                    .doRead();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param dictCode
     * @param value
     * @Desc: 根据dicCode和value查询
     */
    @Override
    public String getDictName(String dictCode, String value) {
        //如果dictName为空
        if(StringUtils.isEmpty(dictCode)){
            //则通过value查询
            Dict dict = dictMapper
                    .selectOne(new QueryWrapper<Dict>().eq("value",value));
            return dict.getName();
        } else {
            //反之，则通过根据dicCode和value查询
            Dict dictByDictCode = dictMapper.selectOne(new QueryWrapper<Dict>().eq("dict_code",dictCode));
            //根据parrent_id和value查询
            Dict dictFinal = dictMapper.selectOne(new QueryWrapper<Dict>()
                    .eq("parent_id", dictByDictCode.getId())
                    .eq("value", value));
            return dictFinal.getName();
        }
    }

    /**
     * @param dictCode
     * @Desc: 根据dictCode查询下级结点
     */
    @Override
    public List<Dict> findByDictCode(String dictCode) {
        //根据dictCode获取对应id
        Dict dictByDictCode = dictMapper
                .selectOne(new QueryWrapper<Dict>().eq("dict_code",dictCode));
        //通过id获取子节点
        List<Dict> childData = this.findChildData(dictByDictCode.getId());
        return childData;
    }


    /**
     * @param response
     * @Desc: 导出数据字典
     */
    @Override
    public void exportDictData(HttpServletResponse response){
        //设置下载信息
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("UTF-8");
        String fileName = "dict";
        response.setHeader("Content-disposition","attachment;filename="+fileName+".xlsx");

        //查询数据库
        List<Dict> dicts = dictMapper.selectList(null);

        //将集合中Dict转换成dictExcel
        List<DictExcelVo> dictExcelVos = new ArrayList<>(dicts.size());
        for (Dict dict : dicts) {
            DictExcelVo dictExcelVo = MapStructs.INSTANCE.toDictExcelVo(dict);
            dictExcelVos.add(dictExcelVo);
        }


        //调用方法，进行写操作
        try {
            EasyExcel.write(response.getOutputStream(), DictExcelVo.class)
                    .sheet("dict")
                    .doWrite(dictExcelVos);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /** 
     * @Desc: 判断id下面是否有子结点、 
     * @Param: id
     * @Return: boolean
     */
    private boolean isChild(Long id){
        Integer parent_id = dictMapper.selectCount(new QueryWrapper<Dict>().eq("parent_id", id));
        return parent_id>0;
    }

}
