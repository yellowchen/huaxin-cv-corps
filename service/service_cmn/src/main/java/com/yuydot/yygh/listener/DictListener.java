package com.yuydot.yygh.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.yuydot.yygh.mapper.DictMapper;
import yygh.mapstruct.MapStructs;
import yygh.pojo.cmn.Dict;
import yygh.vo.cmn.DictExcelVo;


/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 15:57
 */
public class DictListener extends AnalysisEventListener<DictExcelVo> {

    private DictMapper dictMapper;
    public DictListener(DictMapper dictMapper){
        this.dictMapper=dictMapper;
    }
    //一行一行读取excel
    @Override
    public void invoke(DictExcelVo dictExcelVo, AnalysisContext analysisContext) {
        //调用方法将读取数据添加到数据库中
        Dict dict = MapStructs.INSTANCE.toDict(dictExcelVo);
        dict.setIsDeleted(0);
        dictMapper.insert(dict);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
