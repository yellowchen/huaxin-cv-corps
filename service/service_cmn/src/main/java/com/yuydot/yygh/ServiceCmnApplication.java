package com.yuydot.yygh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 9:40
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.yuydot")
public class ServiceCmnApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceCmnApplication.class,args);
    }
}
