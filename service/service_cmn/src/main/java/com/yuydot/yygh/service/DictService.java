package com.yuydot.yygh.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;
import yygh.pojo.cmn.Dict;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @version 1.0
 * @Desc: TODO
 * @Author yu
 * @Date 2023/9/7 10:01
 */
public interface DictService extends IService<Dict> {
    /**
     * @Desc: 根据数据id查询子数据列表
     */
    List<Dict> findChildData(Long id);

    /**
     * @Desc: 导出数据字典
     */
    void exportDictData(HttpServletResponse response) ;

    /**
     * @Desc: 导入数据字典
     */
    void importDictData(MultipartFile file);

    /**
     * @Desc: 根据dicCode和value查询
     */
    String getDictName(String s, String value);

    /**
     * @Desc: 根据dictCode查询下级结点
     */
    List<Dict> findByDictCode(String dictCode);
}
