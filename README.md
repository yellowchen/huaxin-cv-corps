# yygh

### 介绍
    基于微服务分布式医疗挂号系统

### 项目功能
#### 1.1 预约挂号系统前台

- 登录
- 首页信息展示
- 预约挂号
- 支付订单

#### 1.2 预约挂号系统管理员后台

- 数据字典  (easy excel 导入导出)
- 医院管理
- 会员管理
- 订单(预约)管理
- 统计管理

#### 1.3 医院接口模拟平台

相当于医院自己维护的系统，通过签名校验的方式调研我们提供的接口来管理医院数据。


### 2 技术栈

#### 2.1前端

- Vue.js：web界面的渐进式框架
- Node.js：JavaScript运行环境
- Axios
- vue-admin-template 管理员模板
- webpack：打包工具
- nuxt 服务端渲染
- element ui

#### 2.2 后端

**微服务：**

- SpringBoot
- SpringCloud Alibaba 框架
- Nacos 注册中心
- SpringCloud Feign 远程调用
- SpringCloud Gateway 微服务网关
- Maven 子父多模块


**数据库：**

- MySQL : 存储用户、订单、预约等信息
- MongoDB：负责医院基本信息（性能更高）

**中间件：**

- Redis：做缓存，存储验证码
- RabbitMQ：应用解耦，消息通知
- Nginx：部署项目
- Git：代码管理工具

**工具库：**

- Lombok
- MyBatis-Plus
- Swagger2
- easy excel ：读写Excle文件
- Json Web Token：生成jwt token
- Joda Time：日期时间操作