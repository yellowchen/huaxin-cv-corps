package yygh.mapstruct;

import org.mapstruct.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义转换
 */
public class MapStructFormatter {

    // 必须使用的规定格式：自定义转换类型【int -> String】
    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.CLASS)
    public @interface PostTopTranslate {}

}
