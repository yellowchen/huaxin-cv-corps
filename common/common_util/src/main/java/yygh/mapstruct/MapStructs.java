package yygh.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import yygh.pojo.cmn.Dict;
import yygh.pojo.hosp.Department;
import yygh.pojo.hosp.Hospital;
import yygh.pojo.hosp.HospitalSet;
import yygh.pojo.hosp.Schedule;
import yygh.vo.cmn.DictExcelVo;
import yygh.vo.hosp.DepartmentQueryVo;
import yygh.vo.hosp.HospitalQueryVo;
import yygh.vo.hosp.HospitalSetVo;
import yygh.vo.hosp.ScheduleQueryVo;

/**
 * 1、简单Java对象的转换【不用自己写很多 set、get】
 * 2、https://github.com/mapstruct/mapstruct
 */

@Mapper(uses = {
        MapStructFormatter.class
})
public interface MapStructs {
    MapStructs INSTANCE = Mappers.getMapper(MapStructs.class);

    @Mapping(source = "id",target = "id")
    @Mapping(source = "hosname",target = "hosname")
    @Mapping(source = "hoscode",target = "hoscode")
    @Mapping(source = "apiUrl",target = "apiUrl")
    @Mapping(source = "contactsName",target = "contactsName")
    @Mapping(source = "contactsPhone",target = "contactsPhone")
    @Mapping(source = "status",target = "status")
    HospitalSetVo toHospitalSetVo(HospitalSet hospitalSet);

    @Mapping(source = "id",target = "id")
    @Mapping(source = "hosname",target = "hosname")
    @Mapping(source = "hoscode",target = "hoscode")
    @Mapping(source = "apiUrl",target = "apiUrl")
    @Mapping(source = "contactsName",target = "contactsName")
    @Mapping(source = "contactsPhone",target = "contactsPhone")
    @Mapping(source = "status",target = "status")
    HospitalSet toHospitalSet(HospitalSetVo hospitalSetVo);

    @Mapping(source = "id",target = "id")
    @Mapping(source = "parentId",target = "parentId")
    @Mapping(source = "name",target = "name")
    @Mapping(source = "value",target = "value")
    @Mapping(source = "dictCode",target = "dictCode")
    DictExcelVo toDictExcelVo(Dict dict);

    Dict toDict(DictExcelVo dictExcelVo);

    Department toDepartment(DepartmentQueryVo departmentQueryVo);

    Schedule toSchedule(ScheduleQueryVo scheduleQueryVo);

    Hospital toHospital(HospitalQueryVo hospitalQueryVo);

    /*
    1、Po -> vo     【用来将从数据库查到的数据过滤成 vo返回给前端】
    2、可以解决转换类型不匹配、参数名不匹配的问题。 @Mapping 参数如下
    （1）source：源对象
    （2）target：目标对象
    （3）qualifiedBy：找转换器中的方法
    */

}
