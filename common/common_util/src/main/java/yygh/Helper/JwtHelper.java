package yygh.Helper;

import io.jsonwebtoken.*;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @version 1.0
 * @Desc: JWT工具类
 * @Author yu
 * @Date 2023/9/18 22:20
 */
public class JwtHelper {
    /**
     * @Desc: token过期时间
     */
    private static long tokenExpiration = 24 * 60 * 60 * 1000;

    /**
     * @Desc: 秘钥(盐值)，加密token
     */
    private static String tokenSignKey = "yuydot";

    /**
     * @Desc: 通过userId和userName生成token
     */
    public static String createToken(Long userId, String userName) {
        String token = Jwts.builder()
                .setSubject("YYGH-USER")//设置JWT的主题，相当于是分类
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))//设置过期时间
                .claim("userId", userId)//主体信息，包括用户id、用户名称
                .claim("userName", userName)
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)//设置JWT的签名算法和签名密钥
                .compressWith(CompressionCodecs.GZIP)//对JWT进行GZIP压缩
                .compact();//生成最终的JWT字符串
        return token;
    }

    /**
     * @Desc: 根据token解析得到userId
     */
    public static Long getUserId(String token) {
        if (StringUtils.isEmpty(token)) return null;
        Jws<Claims> claimsJws
                = Jwts.parser()
                .setSigningKey(tokenSignKey)
                .parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        Integer userId = (Integer) claims.get("userId");
        return userId.longValue();
    }

    /**
     * @Desc: 根据token解析得到userName
     */
    public static String getUserName(String token) {
        if (StringUtils.isEmpty(token)) return "";
        Jws<Claims> claimsJws
                = Jwts.parser()
                .setSigningKey(tokenSignKey)
                .parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return (String) claims.get("userName");
    }

    /**
     * @Desc: 测试
     */
    public static void main(String[] args) {
        String token = JwtHelper.createToken(1L, "55");
        System.out.println(token);
        System.out.println(JwtHelper.getUserId(token));
        System.out.println(JwtHelper.getUserName(token));
    }
}
