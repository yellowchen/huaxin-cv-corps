package yygh.exception;

import com.yuydot.yygh.result.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @version 1.0
 * @Desc: 自定义全局异常
 * @Author yu
 * @Date 2023/9/5 10:43
 */
@Data
@ApiModel(value = "自定义全局异常类")
public class YyghException extends RuntimeException {

    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对象
     * @param message
     * @param code
     */
    public YyghException(String message,Integer code){
        super(message);
        this.code=code;
    }

    /**
     * 接收枚举类型对象
     * @param codeEnum
     */
    public YyghException(ResultCodeEnum codeEnum){
        super(codeEnum.getMessage());
        this.code=codeEnum.getCode();
    }

    @Override
    public String toString() {
        return "YyghException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}